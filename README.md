# DDI-3.2
```
  ____          _  _      _    ____   ____  ___        _        _____    ____
 | __ )  _   _ (_)| |  __| |  |  _ \ |  _ \|_ _|      | |      |___ /   |___ \
 |  _ \ | | | || || | / _` |  | | | || | | || | _____ | |        |_ \     __) |
 | |_) || |_| || || || (_| |  | |_| || |_| || ||_____|| |___    ___) |_  / __/
 |____/  \__,_||_||_| \__,_|  |____/ |____/|___|      |_____|  |____/(_)|_____|

```

Repository for the documentation of DDI-3.2, developed by Jon Johnson and Wendy Thomas

The documentation of the model in this repository is automagically compiled and published on http://ddi-lifecycle-3-2-documentation.readthedocs.io

This repository contains:

* Documentation (restructured text used to produce html, epub and pdf)

_Python_ and _PIP_, _Sphinx_ and PDF extension to Sphinx, to install:
```
$ sudo apt-get install python python-pip
$ sudo pip install sphinx
$ sudo pip install rst2pdf <- not really working at the moment
```

_Sphinx_, get binaries from http://sphinx-doc.org