==========================================
DDI3.2 Documentation
==========================================

.. image:: ../_static/images/ddi-logo.*


.. toctree::
   :caption: Table of contents
   :maxdepth: 2

   About/index.rst
   otherstandards/index.rst
   generalstructures/index.rst
   structures/index.rst

