`ConceptualVariable <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/conceptualcomponent_xsd/elements/ConceptualVariable.html>`_
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Describes a ConceptualVariable which provides the link between a concept and a specific universe (object) that defines this as a ConceptualVariable. In addition to the standard name, label, and description, it provides the two primary components of a ConceptualVariable by referencing a concept and its related universe. Note that the concept referenced may itself contain sub-concepts and/or references to similar concepts.

::

 ------------------------------------------------------------
 Namespace: cc (ConceptualComponent) 
 Parent Maintainable: ConceptualVariableScheme
 Type: ConceptualVariableType
 ------------------------------------------------------------
 
 ConceptualVariable
  Extension base: Versionable
  ConceptualVariableName                 (0..n)
  r:Label                                (0..n)
  r:Description                          (0..1)
  r:ConceptReference                     (0..1)
  r:UniverseReference                    (0..1)
  
Example
^^^^^^^^

::
