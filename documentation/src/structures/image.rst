`Image <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/complexTypes/ImageType.html>`_
------------------------------------------------------------------------------------------------------------------------------------------------------------

A reference to an image, with a description of its properties and type.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: 
 Type: ImageType
 ------------------------------------------------------------

 Image
 Extension base: None
  ImageLocation
  TypeOfImage                            (0..1)

Example
^^^^^^^
 
::

