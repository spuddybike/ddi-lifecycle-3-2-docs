`Universe <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/conceptualcomponent_xsd/elements/Universe.html>`_
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Describes a universe which may also be known as a population. A Universe describes the "object" of a Data Element Concept or Data Element as defined by ISO/IEC 11179. A universe may be organized into hierarchical sub-universes. In addition to the standard name, label, and description, the universe may provide a generation code (how the universe is differentiated or split out from another universe), a definition of hierarchical sub-settings for the universe, and an attribute that indicates if the description of the universe is stated in terms of what the universe includes.

::

 ------------------------------------------------------------------
 Namespace: cc (ConceptualComponent)
 Parent Maintainable: UniverseSchemeType
 Type: UniverseType
 ------------------------------------------------------------------

 Universe
  Extension base: Versionable
   @isInclusive
   UniverseName	                         (0..n)
   r:Label	                         (0..n)
   r:Description	                 (0..1)
   DefiningConceptReference	         (0..1)
   UniverseGenerationCode	         (0..n)
   CHOICE                                (0..n)
     SubUniverseClass
     SubUniverseClassReference
   END CHOICE

Example
^^^^^^^^^

::
