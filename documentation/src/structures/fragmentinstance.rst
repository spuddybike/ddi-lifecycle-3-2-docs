`FragmentInstance <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/instance_xsd/elements/FragmentInstance.html>`_
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A Fragment Instance is used to transfer maintainable or versionable objects plus any associated notes and other material in response to a query. TopLevelReference provides a record of the reference(s) (from the query) to which the FragmentInstance is responding. The contents of the maintainable and versionable objects are transported as ddi:Fragment entries.

::

 ------------------------------------------------------------
 Namespace: i (Instance)
 Parent Maintainable: Fragment
 Type: FragmentInstanceType

 FragmentInstance
 Extension base: None
  TopLevelReference                      (0..n)
  Fragment                               (0..n)

Example
^^^^^^^
 
::
