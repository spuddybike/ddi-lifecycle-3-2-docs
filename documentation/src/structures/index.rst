=====================
Specific Structures
=====================

.. toctree::
   :maxdepth: 2

   archive.rst
   binding.rst
   budget.rst
   category.rst
   citation.rst
   codelist.rst
   codevalue.rst
   collectionevent.rst
   commandcode.rst
   comparison.rst
   concept.rst
   conceptualcomponent.rst
   conceptualvariable.rst
   controlconstruct.rst
   coverage.rst
   datacollection.rst
   datarelationship.rst
   date.rst
   ddiinstance.rst
   ddiprofile.rst
   embargo.rst
   expostevaluation.rst
   fragmentinstance.rst
   fundinginformation.rst
   geographiclocation.rst
   group.rst
   image.rst
   inparameter.rst
   outparameter.rst
   universe.rst
   










