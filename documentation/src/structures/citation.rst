`Citation <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/elements/Citation.html>`_
---------------------------------------------------------------------------------------------------------------------------------------------------------

Provides a bibliographic citation using a DDI structure that maps to Dublin Core objects.
No object is required in order to support production processes.
It is strongly recommended that at minimum a Title be provided.
A full set of Qualified Dublin Core descriptor may be provided following the DDI Citation elements.
Dublin Core elements should supplement rather than substitute for the DDI Citation elements.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: varies by usage
 Type: CitationType
 ------------------------------------------------------------

 Citation
  Extension base: Versionable
  Title                                  (0..1)
  SubTitle                               (0..n)
  AlternateTitle                         (0..n)
  Creator                                (0..n)
  Publisher                              (0..n)
  Contributor                            (0..n)
  PublicationDate                        (0..1)
  Language                               (0..n)
  InternationalIdentifier                (0..n)
  Copyright                              (0..n)
  dc:any                                 (0..n)

Example
^^^^^^^^

::

  <r:Citation>
    <r:Title>Twelfth Census of the United States Taken in the Year 1990, Census Reports Volume I - Population Part I</r:Title>
    <r:Creator>
      <r:CreatorName affiliation="Interior Department">United States Census Office</r:CreatorName>
    </r:Creator>
    <r:Publisher>
      <r:PublisherName>Washington: United States Census Office</r:PublisherName>
    </r:Publisher>
    <r:PublicationDate>
      <r:SimpleDate>1901</r:SimpleDate>
    </r:PublicationDate>
  </r:Citation>
