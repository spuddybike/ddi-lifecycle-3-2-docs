`Data Relationship <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/logicalproduct_xsd/elements/DataRelationship.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Describes the relationships among logical records in the dataset. Date Relationship is needed to create the appropriate link between the logical record and the physical storage description.

::

 ------------------------------------------------------------
 Namespace: l (logicalproduct)
 Parent Maintainable: LogicalProduct
 Type: DataRelationshipType
 ------------------------------------------------------------
 
 DataRelationship
  Extension base: Versionable
   DataRelationshipName                  (0..n)
   r:Label                               (0..n)
   r:Description                         (0..1)
   LogicalRecord                         (0..n)
   RecordRelationship                    (0..n)

 Example
 ^^^^^^^^
 
 ::
 
  <DataRelationship>
  <Agency xmlns="ddi:reusable:3_2">us.atg</Agency>
  <ID xmlns="ddi:reusable:3_2">DR_1</ID>
  <Version xmlns="ddi:reusable:3_2">1.0</Version>
  <LogicalRecord hasLocator="false">
    <Agency xmlns="ddi:reusable:3_2">us.atg</Agency>
    <ID xmlns="ddi:reusable:3_2">LR_1</ID>
    <Version xmlns="ddi:reusable:3_2">1.0</Version>
    <VariablesInRecord allVariablesInLogicalProduct="true"/>
  </LogicalRecord>
 </DataRelationship>

