`Group <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/group_xsd/elements/Group.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A primary packaging and publication module within DDI containing a Group of StudyUnits. The Group structure allows metadata regarding multiple study units to be published as a structured entity. Studies may be grouped "by design" such as a repeated study with intended areas of commonality between each study, or "ad hoc" where studies are grouped for applied or administrative reasons.

One aspect of DDI Version 3.1 onwards which follows from the support of the whole life cycle is the introduction of groups of studies as the subject for metadata documentation. Longitudinal studies are a good example of this. A longitudinal study is a study that is repeated at specific points in time, and thus represents a group of related studies. These need to be documented as a group in order to clearly document the repurposing of aspects of the initial study and the relationship that exists between each of the component studies in the group. 

The ability to document these complex cases or groups is a major advance of DDI 3. The ÔcomplexÕ case involves a series or collection of studies which are related in some way or a group of studies which are being compared. It is important to recognize which cases are ÔcomplexÕ because they use features of the DDI which are potentially more difficult to understand and implement, such as group inheritance and comparison 

A Group can be comprised of StudyUnits and SubGroups. A standard set of attributes describes the following dimensions for grouping: 

* Time 
* Instrument 
* Panel 
* Geography 
* Datasets 
* Language 

A table providing the specified values and a set of decision trees for determining their value is provided in Appendix 3. Note that in all cases these attributes are providing general information on the relationships between the StudyUnits and SubGroups which comprise the Group (or SubGroup) that are intended to assist the programmer in anticipating the types of comparison or repletion patterns they will need to address. For example, if an individual StudyUnit within a group has content in three languages (labels provided in English, German, and French) this does not make Language a grouping factor. The Language attribute would be set to ÔL0Õ ÔNot a reason for grouping. If the Group consisted of two StudyUnits say the English version of a Health Canada Survey and the French version of that survey, the Language attribute would be L2.

All original languages with full language equivalence as Health Canada considers both versions to be original and each contains the equivalent intellectual content. 

In interpreting the descriptions please note that the term rolling for panel or geography means that panel waves or geographic waves were used. For example there are four panels of respondents each starting at a different point in time and having their own repetition cycle. In panel studies this usually means a new panel wave is started each year and each panel is surveyed yearly for a limited number of years. For geography this means that there are geographic panels each consisting of say one quarter of the total Metropolitan Areas in the United States. 

A survey takes place yearly but the first year they survey only one geographic panel and each geographic panel is surveyed every four years. In this way the entire set of Metropolitan Areas is surveyed every four years.

::

 ------------------------------------------------------------
 Namespace: g (Group)
 Parent Maintainable: varies by usage
 Type: GroupType
 ------------------------------------------------------------

 Group
 Extension base: Maintainable
  @time
  @captureinstrument
  @panel
  @geography
  @dataProduct
  @languageRelationship
  @userDefinedGroupProperty
  @userDefinedGroupPropertyValue
  r:Citation                             (0..1)
  r:Abstract                             (0..1)
  r:AuthorizationSource                  (0..n)
  r:UniverseReference                    (0..1)
  r:SeriesStatement                      (0..n)
  r:QualityStatementReference            (0..n)
  CHOICE                                 (0..n) 
   r:QualityStatementScheme              (0..n) 
   r:QualityStatementSchemeReference     (0..n)
  END CHOICE    
  r:ExPostEvaluation                     (0..n)
  r:FundingInformation                   (0..n)
  ProjectBudget                          (0..n)
  r:Purpose                              (0..1)
  r:Coverage                             (0..1)
  r:AnalysisUnit                         (0..n)
  r:AnalysisUnitsCovered                 (0..1)
  r:KindOfData                           (0..n)
  r:OtherMaterial                        (0..n)
  r:RequiredResourcePackages             (0..1)
  r:Embargo                              (0..n)
  CHOICE                                 (0..n)
   c:ConceptualComponent 
   r:ConceptualComponentReference
  END CHOICE    
  CHOICE                                 (0..n)
   d:DataCollection 
   r:DataCollectionReference
  END CHOICE
  CHOICE                                 (0..n)
   l:BaseLogicalProduct 
   r:LogicalProductReference
  END CHOICE
  CHOICE                                 (0..n)
   p:PhysicalDataProduct 
   r:PhysicalDataProductReference
  END CHOICE
  CHOICE                                 (0..n)
   pi:PhysicalInstance 
   r:PhysicalInstanceReference
  END CHOICE
  CHOICE                                 (0..n)
   a:Archive 
   r:ArchiveReference
   END CHOICE    
  CHOICE                                 (0..n)
   pr:DDIProfile 
   r:DDIProfileReference
  END CHOICE
  CHOICE
   cm:Comparison 
   r:ComparisonReference
  END CHOICE                             (0..n)
  CHOICE                                 (0..n)
   s:StudyUnit 
   r:StudyUnitReference
  END CHOICE
  CHOICE                                 (0..n)
   SubGroup 
   SubGroupReference
  END CHOICE

Example
^^^^^^^
 
::

