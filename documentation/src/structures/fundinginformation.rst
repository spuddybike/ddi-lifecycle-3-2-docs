`Funding Information <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/complexTypes/FundingInformationType.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Provides information about the agency and grant(s) which funded the described entity.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: varies by usage
 Type: FundingInformationType
 ------------------------------------------------------------

 FundingInformation
  Extension base: None
  AgencyOrganizationReference            (0..n)
  FunderRole                             (0..1)
  GrantNumber                            (0..n)
  Description                            (0..1)

Example
^^^^^^^
 
::

 <r:FundingInformation>
  <r:AgencyOrganizationReference>
   <r:Agency>uk.cls.bcs70</r:Agency>
   <r:ID>78a563d7-dbe1-4f13-93a9-69844a3bee88</r:ID>
   <r:Version>1</r:Version>
   <r:TypeOfObject>Organization</r:TypeOfObject>
  </r:AgencyOrganizationReference>
  <r:GrantNumber>RES-579-47-0001</r:GrantNumber>
 </r:FundingInformation>
