`InParameter <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/elements/InParameter.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A parameter that may accept content from outside its parent element. For instance a variable has in InParameter from a question, binding is used to link the OutParameter of a Question to the InParameter of a GenerationInstruction.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: varies by usage
 Type: InParameterType

 InParameter
 Extension base: Identifiable
  @isArray
  @limitArrayIndex
  ParameterName                        (0..n)
  Alias                                (0..1)
  Description                          (0..1)
  CHOICE                               (0..1)
   ValueRepresentation 
   ValueRepresentationReference
  END CHOICE 
  DefaultValue                         (0..1)

Example
^^^^^^^

The use of an inParameter in a more complex example is shown in :ref:`inout`

::

 <r:InParameter isIdentifiable="true" scopeOfUniqueness="Agency" isArray="false">
  <r:URN>urn:ddi:us.mpc:GI_Age:1  </r:URN>
  <r:Alias>AGE  </r:Alias>
 </r:InParameter>
