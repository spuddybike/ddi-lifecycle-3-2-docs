`OutParameter <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/elements/OutParameter.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Outparameter does not contain a limitArrayIndex attribute. Because while the OutParameter may be an array, only the InParameter needs to limit the input to a specific item in the array. For example, a question on the age of a person which is repeated for each person in a household would result in an array of ages as output. The variable noting the age of person 3 would only use the 3rd item in the array
A parameter that contains output from its parent object, such as the specific response value of a question.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: varies by usage
 Type: ParameterType
 ------------------------------------------------------------

 OutParameter
 Extension base: Identifiable
  @isArray 
  ParameterName                          (0..n)
  Alias                                  (0..1)
  Description                            (0..1)
  CHOICE                                 (0..1)
   ValueRepresentation 
   ValueRepresentationReference
  END CHOICE 
  DefaultValue                           (0..1)

Example
^^^^^^^

The use of an OutParameter in a more complex example is shown in :ref: `inout`

::

 <r:OutParameter isIdentifiable="true" scopeOfUniqueness="Agency" isArray="false">
  <r:URN>urn:ddi:us.mpc:GI_Age_Cohort:1</r:URN>
  <r:Alias>AGE_5</r:Alias>
 </r:OutParameter>

