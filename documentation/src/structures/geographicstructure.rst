`Geographic Structure <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/elements/GeographicStructure.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Contains information on the hierarchy of the geographic structure.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: GeographicStructureScheme
 Type: GeographicStructureType
 ------------------------------------------------------------

 GeographicStructure
 Extension base: Versionable
  GeographicStructureName                (0..n)
  Label                                  (0..n)
  Description                            (0..1)
  AuthorizedSource                       (0..n)
  CHOICE                                 (0..n)
   GeographicLevel 
   GeographicLevelReference
  END CHOICE    

Example
^^^^^^^
 
::
