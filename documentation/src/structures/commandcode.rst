`CommandCode <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/elements/CommandCode.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------

Content of the command itself expressed in the language specified in the parent object.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: varies by usage
 Type: CommandTypeCode
 ------------------------------------------------------------

 CommandCode
 Extension base: None
  Description                            (0..1)
  Command                                (0..n)
  CommandFile                            (0..n)
  StructuredCommand                      (0..1)

Example
^^^^^^^

The use of CommandCode within a more complex example is shown in :ref: `inout`

::

 <r:CommandCode>
  <r:Command>
   <r:ProgramLanguage>SPSS  </r:ProgramLanguage>
   <r:InParameter isIdentifiable="true" scopeOfUniqueness="Agency" isArray="false">
    <r:URN>urn:ddi:us.mpc:GI_Age:1  </r:URN>
    <r:Alias>AGE  </r:Alias>
   </r:InParameter>
   <r:OutParameter isIdentifiable="true" scopeOfUniqueness="Agency" isArray="false">
    <r:URN>urn:ddi:us.mpc:GI_Age_Cohort:1  </r:URN>
     <r:Alias>AGE_5  </r:Alias>
   </r:OutParameter>
   <r:Binding>
    <r:SourceParameterReference isReference="true" isExternal="false" lateBound="false">
     <r:URN>urn:ddi:us.mpc:QC_OUT_2:1  </r:URN>
     <r:TypeOfObject>OutParameter  </r:TypeOfObject>
    </r:SourceParameterReference>
    <r:TargetParameterReference isReference="true" isExternal="false" lateBound="false">
     <r:URN>urn:ddi:us.mpc:GI_Age:1  </r:URN>
     <r:TypeOfObject>InParameter  </r:TypeOfObject>
    </r:TargetParameterReference>
   </r:Binding>
   <r:CommandContent>If (AGE &amp;lt; 5) AGE_5=1; If (AGE &amp;gt;=5) &amp; (AGE &amp;lt; 10) AGE_5=2; If (AGE &amp;gt;=10 &amp; (AGE &amp;lt; 15) AGE_5=3; If (AGE &amp;gt;=15 &amp; (AGE &amp;lt; 20) AGE_5=4; If (AGE &amp;gt;=20 AGE_5=5  </r:CommandContent>
  </r:Command>
 <r:CommandCode>

