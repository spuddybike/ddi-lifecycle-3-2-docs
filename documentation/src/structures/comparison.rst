`Comparison <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/comparative_xsd/complexTypes/ComparisonType.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A maintainable module containing maps between objects of the same or similar type. Maps allow for pair-wise mapping of two objects by describing their similarities and differences in order to make assertions regarding their comparability. Currently maps allow for the comparison of concepts, variables, questions, categories, universes, and representations that have managed content (code, category, numeric, text, datetime and scale).

::

 ------------------------------------------------------------
 Namespace: c (Comparative)
 Parent Maintainable: StudyUnit, Group, or ResourcePackage
 Type: ComparisonType
 ------------------------------------------------------------

 Comparison
  Extension base: Maintainable
   ComparisonName	                     (0..n)
   r:Label	                             (0..n)
   r:Description	                     (0..1)
   CHOICE	                             (0..n)
     ConceptMap
     ConceptMapReference
   END CHOICE
   CHOICE
     VariableMap
     VariableMapReference
   END CHOICE
   CHOICE	                             (0..n)
     QuestionMap
     QuestionMapReference
   END CHOICE
   CHOICE	                             (0..n)
     CategoryMap
     CategoryMapReference
   END CHOICE
   CHOICE	                             (0..n)
     RepresentationMap
     RepresentationMapReference
   END CHOICE
   CHOICE	                             (0..n)
     UniverseMap
     UniverseMapReference
   END CHOICE

Example
^^^^^^^^

::
