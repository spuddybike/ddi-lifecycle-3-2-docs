`DDI Instance <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/instance_xsd/elements/DDIInstance.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------

DDIInstance is the top-level publication wrapper for any DDI document. All DDI content published as XML (with the exception of a Fragment intended for transmission) has DDIInstance as its top level structure. In addition to a citation and coverage statement for the instance, the DDIInstance can contain a Group, ResourcePackage, LocalHoldingPackage or StudyUnit.

::

 ------------------------------------------------------------
 Namespace: I (Instance)
 Parent Maintainable: varies by usage
 Type: DDIInstanceType
 ------------------------------------------------------------

 DDIInstance
  Extension base: Maintainable
  r:Citation                             (0..1)
  r:Coverage                             (0..1)
  CHOICE                                 (0..n) 
   g:Group 
   r:GroupReference
  END CHOICE    
  CHOICE                                 (0..n)
   g:ResourcePackage 
   r:ResourcePackageReference
  END CHOICE    
  CHOICE                                 (0..n)
   g:LocalHoldingPackage 
   r:LocalHoldingPackageReference
  END CHOICE    
  CHOICE                                 (0..n)
   s:StudyUnit 
   r:StudyUnitReference
  END CHOICE    
  r:OtherMaterial                        (0..n)
  CHOICE                                 (0..n)
   pr:DDIProfile 
   r:DDIProfileReference
  END CHOICE    
  TranslationInformation                 (0..1)

Example
^^^^^^^
 
::

