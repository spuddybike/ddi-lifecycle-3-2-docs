`DataCollection <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/datacollection_xsd/elements/DataCollection.html>`_
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A maintainable module containing information on activities related to data collection/capture and the processing required for the creation a data product. This section covers the methodologies, events, data sources, collection instruments and processes which comprise the collection/capture and processing of data. Methodology covers approaches used for selecting samples, administering surveys, timing repeated data collection activities. Collection Event specifies data sources, collection instruments, questions and question flow, and data processing activities. This module houses Processing Instructions (General Instructions and Generation Instructions) which may be referenced by variables or comparison maps. It houses the following schemes: Question Scheme, Control Construct Scheme (questionnaire flow), Interviewer Instruction Scheme, Instrument Scheme, Processing Event Scheme, and Processing Instruction Scheme.

::

 ------------------------------------------------------------
 Namespace: d (DataCollection)
 Parent Maintainable: varies according to usage
 Type: DataCollectionType
 ------------------------------------------------------------
 
 DataCollection
  Extension base: Maintainable
  DataCollectionModuleName               (0..n)
  r:Label                                (0..n)
  r:Description                          (0..1)
  r:Coverage                             (0..1)
  r:OtherMaterial                        (0..n)
  CHOICE                                 (0..1)
   Methodology 
   MethodologyReference
  END CHOICE    
  CollectionEvent                        (0..n)
  CHOICE                                 (0..n)
   QuestionScheme 
   r:QuestionSchemeReference
  END CHOICE    
  CHOICE                                 (0..n)
   ControlConstructScheme 
   r:ControlConstructSchemeReference
  END CHOICE
  CHOICE                                 (0..n)
   InterviewerInstructionScheme 
   r:InterviewerInstructionSchemeReference
  END CHOICE    
  CHOICE                                 (0..n)
   InstrumentScheme 
   r:InstrumentSchemeReference
  END CHOICE    
  CHOICE                                 (0..n)
   ProcessingEventScheme 
   ProcessingEventSchemeReference
  END CHOICE    
  CHOICE                                 (0..n)
   ProcessingInstructionScheme 
   ProcessingInstructionSchemeReference
  END CHOICE
 
 Example
 ^^^^^^^^
 
 ::

<DataCollection isUniversallyUnique="true" versionDate="2016-06-09T10:32:52.7829559Z">
 <r:URN>urn:ddi:uk.iser:56acc464-6368-4405-ac28-0195a21c1f19:6</r:URN>
 <DataCollectionModuleName>
  <r:String xml:lang="en-GB">us1_ysc</r:String>
  </DataCollectionModuleName>
   <r:Label>
    <r:Content xml:lang="en-GB">Youth self-completion questionnaire</r:Content>
   </r:Label>
  <CollectionEvent isUniversallyUnique="true">
   <r:URN>urn:ddi:uk.iser:094e05f4-c29b-4c11-ba6c-2e055ea712f0:6</r:URN>
   <DataCollectorOrganizationReference>
    <r:Agency>uk.iser</r:Agency>
    <r:ID>fdd368a5-7095-4f65-bfb4-27c2833c91dd</r:ID>
    <r:Version>1</r:Version>
    <r:TypeOfObject>Organization</r:TypeOfObject>
   </DataCollectorOrganizationReference>
   <DataCollectorOrganizationReference>
    <r:Agency>uk.iser</r:Agency>
    <r:ID>0735accf-16ec-4381-aa0c-e69d53af51da</r:ID>
    <r:Version>1</r:Version>
    <r:TypeOfObject>Organization</r:TypeOfObject>
   </DataCollectorOrganizationReference>
   <DataCollectionDate>
    <r:StartDate>2009-01</r:StartDate>
    <r:EndDate>2010-12</r:EndDate>
   </DataCollectionDate>
   <ModeOfCollection isUniversallyUnique="true">
    <r:URN>urn:ddi:uk.iser:a1679258-18fb-43dd-85b0-e45656910094:6</r:URN>
    <TypeOfModeOfCollection codeListVersionID="1.0">SelfAdministeredQuestionnaire.Paper</TypeOfModeOfCollection>
    <r:Description>
      <r:Content xml:lang="en-GB">Self-administered questionnaire using a traditional paper questionnaire.</r:Content>
    </r:Description>
   </ModeOfCollection>
  </CollectionEvent>
 </DataCollection>
 
 Example
 ^^^^^^^^
 
 ::
 
 <d:DataCollection isMaintainable="true" typeOfIdentifier="Canonical" scopeOfUniqueness="Maintainable">
  <r:URN>urn:ddi:us.archive:DataColl_1:1</r:URN>
  <d:CollectionEvent isIdentifiable="true" typeOfIdentifier="Canonical" scopeOfUniqueness="Maintainable">
   <r:URN>urn:ddi:us.archive:DataColl_1.CE_1:1</r:URN>
   <r:QualityStatementReference isReference="true" isExternal="true" lateBound="false" typeOfIdentifier="Canonical">
     <r:URN>urn:ddi:us.archive:QScheme_1.QS_2:1</r:URN>
     <r:TypeOfObject>QualityStatement</r:TypeOfObject>
    </r:QualityStatementReference>
  </d:CollectionEvent>
 </d:DataCollection>
