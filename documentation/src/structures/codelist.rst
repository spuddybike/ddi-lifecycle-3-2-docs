`Code List <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/logicalproduct_xsd/elements/CodeList.html>`_
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A structure used to associate a list of code values to specified categories. May be flat or hierarchical.

::

 ------------------------------------------------------------
 CodeList
 Namespace: l (logicalproduct)
 Parent Maintainable: CodeListScheme
 Type: CodeListType
 ------------------------------------------------------------

 Code
  Extension base: Maintainable
   CodeListName                          (0..n)
   r:Label                               (0..n)
   r:Description                         (0..1)
   r:RecommendedDataType                 (0..1)
   r:CodeListReference                   (0..n)
   r:CategorySchemeReference             (0..1)
   HierarchyType                         (0..1)
   Level                                 (0..n)
   Code                                  (0..n)

Example
^^^^^^^^

::

  <l:CodeList>
    <r:URN>urn:ddi:uk.cls.mcs:mcs5_sc-cl-000035:1.0.0</r:URN>
    <r:Label>
      <r:Content xml:lang="en-GB">cs_q22_Y</r:Content>
    </r:Label>
    <l:Code>
      <r:URN>urn:ddi:uk.cls.mcs:mcs5_sc-co-000160:1.0.0</r:URN>
      <r:CategoryReference>
        <r:URN>urn:ddi:uk.cls.mcs:mcs5_sc-ca-000102:1.0.0</r:URN>
        <r:TypeOfObject>Category</r:TypeOfObject>
      </r:CategoryReference>
      <r:Value>1</r:Value>
    </l:Code>
    <l:Code>
      <r:URN>urn:ddi:uk.cls.mcs:mcs5_sc-co-000161:1.0.0</r:URN>
      <r:CategoryReference>
        <r:URN>urn:ddi:uk.cls.mcs:mcs5_sc-ca-000103:1.0.0</r:URN>
        <r:TypeOfObject>Category</r:TypeOfObject>
      </r:CategoryReference>
      <r:Value>2</r:Value>
    </l:Code>
    <l:Code>
      <r:URN>urn:ddi:uk.cls.mcs:mcs5_sc-co-000162:1.0.0</r:URN>
      <r:CategoryReference>
        <r:URN>urn:ddi:uk.cls.mcs:mcs5_sc-ca-000104:1.0.0</r:URN>
        <r:TypeOfObject>Category</r:TypeOfObject>
      </r:CategoryReference>
      <r:Value>3</r:Value>
    </l:Code>
  </l:CodeList>
