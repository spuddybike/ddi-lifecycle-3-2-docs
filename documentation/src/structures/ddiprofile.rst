`DDI Profile <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/ddiprofile_xsd/complexTypes/DDIProfileType.html>`_
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Describes the subset of valid DDI objects used by an agency for a specified purpose.
DDI Profile is a simple collection of XPaths that describe the object within DDI that are either used or not use for particular purposes. For example CESSDA can provide a DDI profile denoting which fields it used for its online catalog and can change fields that are ÔoptionalÕ in DDI to ÔrequiredÕ for CESSDA. Objects can be included or excluded as long as the DDI requirements are not violated. Included items can be set to a fixed or default value where appropriate or be provided with an alternate name. This structure facilitates sharing by clearly stating what is expected in the DDI metadata received or sent by an organization and defines what parts of DDI an organization or system can handle. For example software that can handle microdata structures but not NCubes.

::

 ------------------------------------------------------------
 Namespace: pr (ddiprofile)
 Parent Maintainable: varies by usage
 Type: DDIProfileType
 ------------------------------------------------------------

 DDIProfile
 Extension base: Maintainable
  DDIProfileName                         (0..n)
  r:Label                                (0..n)
  r:Description                          (0..1)
  ApplicationOfProfile                   (0..n)
  r:Purpose                              (0..1)
  XPathVersion
  DDINamespace                           (0..1)
  XMLPrefixMap                           (0..n)
  Instructions                           (0..1)
  CHOICE                                 (0..n)
   Used 
   NotUsed
  END CHOICE    

Example
^^^^^^^
 
::

