`Concept <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/conceptualcomponent_xsd/elements/Concept.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

The element Concept is designed to hold a concept than can be represented independently of any particular representation as per the definition is ISO/IEC 11179. A concept may be organized into hierarchical sub-concepts and provides a structure for comparison (using SimilarConcept) to disambiguate concepts.

::

 ------------------------------------------------------------
 Namespace: cc (ConceptualComponent)
 Parent Maintainable: ConceptScheme
 Type: ConceptType
 ------------------------------------------------------------

 Concept
  Extension base: Identifiable
   ConceptName	                         (0..n)
   r:Label	                             (0..n)
   r:Description	                     (0..1)
   SimilarConcept                        (0..n)
   SubclassOfReference	                 (0..n)

Example
^^^^^^^^

::