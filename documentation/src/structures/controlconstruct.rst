`ControlConstruct <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/datacollection_xsd/complexTypes/ControlConstructType.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Provides the basic, extensible structure for control elements used in describing flow logic within the instrument.
Control constructs are the elements that make up the flow logic of a data collection instrument. The various types include Sequence, StatementItem, QuestionConstruct, IfThenElse, RepeatUntil, RepeatWhile and Loop. As a scheme, the individual control constructs as well as master sequences can be held separately and used by a variety of instruments such as Blaise, CPSPro, CASES, and paper products. May also be used to describe Generation Instructions

::

 ------------------------------------------------------------
 Namespace: d (datacollection)
 Parent Maintainable: ControlConstructScheme
 Type: ControlConstructType
 ------------------------------------------------------------

 ControlConstruct
  Extension base: Versionable
   ConstructName                          (0..n)
   r:Label                                (0..n)
   r:Description                          (0..1)
   r:InParameter                          (0..n)
   r:OutParameter                         (0..n)
   r:Binding                              (0..n)
   ExternalAid                            (0..n)
   CHOICE                                 (0..n)
    ExternalInterviewerInstruction 
    InterviewerInstructionReference
   END CHOICE    

Example
^^^^^^^^

::
