`Embargo <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/complexTypes/EmbargoType.html>`_
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Provides information about data that are not currently available because of policies established by the principal investigators and/or data producers.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: varies by usage
 Type: EmbargoType
 ------------------------------------------------------------

 Embargo
 Extension base: Identifiable
  EmbargoName                            (0..n)
  Label                                  (0..n)
  Description                            (0..1)
  Date                                   (0..1)
  Rationale                              (0..1)
  AgencyOrganizationReference            (0..1)
  EnforcementAgencyOrganizationReference (0..n)

Example
^^^^^^^
 
::
