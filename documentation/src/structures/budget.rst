`Budget <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/elements/Budget.html>`_
---------------------------------------------------------------------------------------------------------------------------------------------------------

A description of the budget that can contain a reference to an external budget document.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: ArchiveSpecific
 Type: BudgetType
 ------------------------------------------------------------

 Budget
  Extension base: None
   Description                           (0..1)
   BudgetDocument                        (0..n)
