`Code Value <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/elements/Keyword.html>`_
----------------------------------------------------------------------------------------------------------------------------------------------------------------

Allows for string content which may be taken from an externally maintained controlled vocabulary (code value).
If the content is from a controlled vocabulary provide the code value, as well as a reference to the code list from which the value is taken. Provide as many of the identifying attributes as needed to adequately identify the controlled vocabulary. Note that DDI has published a number of controlled vocabularies applicable to several locations using the CodeValue structure. Use of shared controlled vocabularies helps support interoperability and machine actionability.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: Not applicable
 Type:CodeValueType
 ------------------------------------------------------------

 CodeValueType
  Extension base: None
   @codelistID                           (optional)
   @codelistName                         (optional)
   @codeListAgencyName                   (optional)
   @codeListVersionID                    (optional)
   @otherValue                           (optional)
   @codeListURN                          (optional)
   @codeListSchemeURN                    (optional)

Example
^^^^^^^^

::