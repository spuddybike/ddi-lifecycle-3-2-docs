`Date <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/elements/Date.html>`_
---------------------------------------------------------------------------------------------------------------------------------------------------

A single point in time, a duration, or a time range with start and/or end dates.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: Not applicable
 Type: DateType
 ------------------------------------------------------------
 
 Date
  Extension base: None
  CHOICE
   SimpleDate
   HistoricalDate                        (0..1)
  END CHOICE 
  CHOICE
   StartDate
   HistoricalStartDate                   (0..1)
   EndDate                               (0..1)
   HistoricalEndDate                     (0..1)
   Cycle                                 (0..1)
  END CHOICE 
  CHOICE
   EndDate
   HistoricalEndDate                     (0..1)
  END CHOICE

Example (SimpleDate)
^^^^^^^^^^^^^^^^^^^^
 
::

 <r:PublicationDate>
  <r:SimpleDate>2013-02-26</r:SimpleDate>
  <r:HistoricalDate>
   <r:NonISODate>26 February 2013</r:NonISODate>
   <r:HistoricalDateFormat>dd Month yyyy</r:HistoricalDateFormat>
   <r:Calendar>Gregorian</r:Calendar>
  </r:HistoricalDate>
 </r:PublicationDate>

Example (Complete Date Range)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 
::

 <d:DataCollectionDate>
  <r:StartDate>2010-06-05T20:20:00</r:StartDate>
  <r:HistoricalStartDate>
   <r:NonISODate>24 Sivan 2770</r:NonISODate>
   <r:HistoricalDateFormat>dd Month yyyy</r:HistoricalDateFormat>
   <r:Calendar>Jewish</r:Calendar>
  </r:HistoricalStartDate>
  <r:EndDate>2010-08-05T20:20:00</r:EndDate>
  <r:HistoricalEndDate>
   <r:NonISODate>28 Sivan 5770</r:NonISODate>
   <r:HistoricalDateFormat>dd Month yyyy</r:HistoricalDateFormat>
   <r:Calendar>Jewish</r:Calendar>
  </r:HistoricalEndDate>
 </d:DataCollectionDate>

Example (Range with unknown end date)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 

::

 <r:GeographicTime>
  <r:StartDate>2013-02-26</r:StartDate>
  <r:HistoricalStartDate>
   <r:NonISODate>26 February 2013</r:NonISODate>
   <r:HistoricalDateFormat>dd Month yyyy</r:HistoricalDateFormat>
   <r:Calendar>Gregorian</r:Calendar>
  </r:HistoricalStartDate>
 </r:GeographicTime >

Example (Range with unknown start date)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 

::

 <d:ValidPeriod>
  <r:EndDate>2013-02-26</r:SimpleDate>
  <r:HistoricalEndDate>
   <r:NonISODate>February 26, 2013</r:NonISODate>
  <r:HistoricalDateFormat>Month dd, yyyy</r:HistoricalDateFormat>
   <r:Calendar>Gregorian</r:Calendar>
  </r:HistoricalEndDate>
 </d:ValidPeriod >
