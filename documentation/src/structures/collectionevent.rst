`Collection Event <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/datacollection_xsd/elements/CollectionEvent.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Information on a specific data collection event

::

 ------------------------------------------------------------
 Namespace: d (datacollection)
 Parent Maintainable: DataCollection
 Type: DataCollectionType
 ------------------------------------------------------------

 CollectionEvent
  Extension base: Identifiable
   DataCollectorOrganizationReference    (0..n)
   DataSource                            (0..n)
   DataCollectionDate                    (0..1)
   DataCollectionFrequency               (0..n)
   ModeOfCollection	                     (0..n)
   InstrumentReference                   (0..n)
   CollectionSituation                   (0..n)
   ActionToMinimizeLosses                (0..n)
   r:QualityStatementReference 	         (0..n)

Example
^^^^^^^^

::

  <d:CollectionEvent isIdentifiable="true" typeOfIdentifier="Canonical" scopeOfUniqueness="Maintainable">
    <r:URN>urn:ddi:us.archive:DataColl_1.CE_1:1</r:URN>
    <r:QualityStatementReference isReference="true" isExternal="true" lateBound="false" typeOfIdentifier="Canonical">
      <r:URN>urn:ddi:us.archive:QScheme_1.QS_2:1</r:URN>
      <r:TypeOfObject>QualityStatement</r:TypeOfObject>
    </r:QualityStatementReference>
  </d:CollectionEvent>
