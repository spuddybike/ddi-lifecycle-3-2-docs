`Geographic Location <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/complexTypes/GeographicLocationType.html>`_
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Describes specific instances of GeographicLocations associated with a specified GeographicLevel in a GeographicStructure.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: varies by usage
 Type: GeographicLocationType
 ------------------------------------------------------------

 GeographicLocation
 Extension base: Versionable
  GeographicLocationName                 (0..n)
  Label                                  (0..n)
  Description                            (0..1)
  CHOICE                                 (0..1)
   GeographicLevelReference 
   GeographicLevelDescription
  END CHOICE
  AuthorizedSource                       (0..n)
  LocationValue                          (0..n)

Example
^^^^^^^
 
::
