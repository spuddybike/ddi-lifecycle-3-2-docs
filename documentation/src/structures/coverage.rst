`Coverage <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/complexTypes/CoverageType.html>`_
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Describes the temporal, spatial and topical coverage.

::

 ------------------------------------------------------------
 Namespace: r (reusable) 
 Parent Maintainable: varies by usage
 Type: CoverageType
 ------------------------------------------------------------

 Coverage
  @isRestrictionOfParentCoverage
  CHOICE                              (0..1)
   TopicalCoverageReference 
   TopicalCoverage
  END CHOICE    
  CHOICE                              (0..1)
  SpatialCoverageReference 
  SpatialCoverage
  END CHOICE    
  CHOICE                              (0..1)
   TemporalCoverageReference 
   TemporalCoverage
  END CHOICE    
  RestrictionProcess                 (0..1)
 
Example
^^^^^^^^

::

 <r:Coverage>
  <r:SpatialCoverage isUniversallyUnique="true">
   <r:URN>urn:ddi:uk.iser:e600fee4-a5ad-4c9e-a912-67c5540e4701:10</r:URN>
   <r:Country>GB-ENG</r:Country>
   <r:Country>GB-WLS</r:Country>
   <r:Country>GB-SCT</r:Country>
   <r:Country>GB-NIR</r:Country>
   <r:TopLevelReference>
    <r:LevelName>Country</r:LevelName>
   </r:TopLevelReference>
   <r:LowestLevelReference>
    <r:LevelName>EUL: Government Office Region, Special License: Lower Layer Super Output Areas, Secure Access: National Grid Reference </r:LevelName>
   </r:LowestLevelReference>
   </r:SpatialCoverage>
   <r:TemporalCoverage isUniversallyUnique="true">
    <r:URN>urn:ddi:uk.iser:49fb8416-5a84-4b7f-95cf-6dcad96b23a8:10</r:URN>
    <r:ReferenceDate>
     <r:StartDate>2009-01-08T00:00:00Z</r:StartDate>
     <r:EndDate>2011-03-07T00:00:00Z</r:EndDate>
    </r:ReferenceDate>
  </r:TemporalCoverage>
 </r:Coverage>
