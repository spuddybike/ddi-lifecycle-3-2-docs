`Ex-Post Evaluation <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/complexTypes/ExPostEvaluationType.html>`_
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Evaluation for the purpose of reviewing the study, data collection, data processing, or management processes.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: varies by usage
 Type: ExPostEvaluationType
 ------------------------------------------------------------

 ExPostEvaluation
 Extension base: None
  TypeOfEvaluation                       (0..n)
  Evaluator                              (0..n)
  EvaluationProcess                      (0..n)
  Outcomes                               (0..n)

Example
^^^^^^^
 
::
