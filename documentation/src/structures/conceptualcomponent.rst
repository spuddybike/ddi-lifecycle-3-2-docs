`ConceptualComponent <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/conceptualcomponent_xsd/elements/ConceptualComponent.html>`_
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

A maintainable module for the conceptual components of the study or group of studies. Conceptual components include the objects used to describe the concepts the study is examining, the universe (population) and sub-universes those concepts to which they are related, and the geographic structures and locations wherein those populations reside. Concepts, and ConceptualVariables (containing a concept linked to a universe) provide an abstract view of what is being measured by questions or other forms of data capture, and the variables which are used to describe the data that will be collected. Universe describes the populations (objects) about whom information is sought. GeographicStructure and GeographicLocation specify the geographical locations of those objects and the structural relationships between locations of different types, e.g. the relationship of a city to the state that contains it. In addition to the standard name, label, and description, ConceptualComponent contains ConceptSchemes, ConceptualVariableSchemes, UniverseSchemes, GeographicStructureSchemes, and GeographicLocationSchemes both in-line and by reference.

::

 ------------------------------------------------------------
 Namespace: cc (ConceptualComponent) 
 Parent Maintainable: varies by usage
 Type: ConceptualComponentType
 ------------------------------------------------------------

 ConceptualComponent
  Extension base: Maintainable
  ConceptualComponentModuleName          (0..n)
  r:Label                                (0..n)
  r:Description                          (0..1)
  r:Coverage                             (0..1)
  r:OtherMaterial                        (0..n)
  CHOICE                                 (0..n)
   ConceptScheme 
   r:ConceptSchemeReference
  END CHOICE    
  CHOICE                                 (0..n)
   UniverseScheme 
   r:UniverseSchemeReference
  END CHOICE    
  CHOICE                                 (0..n)
   ConceptualVariableScheme 
   r:ConceptualVariableSchemeReference
  END CHOICE    
  CHOICE                                 (0..n)
   GeographicStructureScheme 
   r:GeographicStructureSchemeReference
  END CHOICE    
  CHOICE                                 (0..n)
   GeographicLocationScheme 
   r:GeographicLocationSchemeReference
  END CHOICE    

Example
^^^^^^^^

::
