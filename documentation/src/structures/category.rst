`Category <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/logicalproduct_xsd/elements/Category.html>`_
----------------------------------------------------------------------------------------------------------------------------------------------------------------

A description of a particular category or response.

::

 ------------------------------------------------------------
 Namespace: l (logicalproduct)
 Parent Maintainable: CategoryScheme
 Type: CategoryType
 ------------------------------------------------------------

 Category
  Extension base: Versionable
   @isMissing
   CategoryName	                         (0..n)
   r:Label                               (0..n)
   r:Description                         (0..1)
   r:ConceptReference	                 (0..1)
   Generation	                         (0..1)
   SubCategoryReference	                 (0..n)

Example
^^^^^^^^

::

  <l:Category>
   <r:URN>urn:ddi:uk.cls.mcs:mcs5_sc-ca-000102:1.0.0</r:URN>
   <l:CategoryName>
     <r:String xml:lang="en-GB">102</r:String>
   </l:CategoryName>
   <r:Label>
    <r:Content xml:lang="en-GB">I wish my family could afford to buy me
      more of what I want</r:Content>
    </r:Label>
  </l:Category>