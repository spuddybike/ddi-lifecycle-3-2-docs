`Binding <http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/elements/Binding.html>`_
---------------------------------------------------------------------------------------------------------------------------------------------------------

A structure used to bind the content of a parameter declared as the source to a parameter declared as the target. For isntance, linking an InParameter to an OutParameter, binding a question to a variable.

::

 ------------------------------------------------------------
 Namespace: r (reusable)
 Parent Maintainable: varies by usage
 Type: BindingType
 ------------------------------------------------------------
 
Example
^^^^^^^

The use of Binding in a more complex example is shown in :ref: `inout`

::

 <r:Binding>
  <r:SourceParameterReference isReference="true" isExternal="false" lateBound="false">
   <r:URN>urn:ddi:us.mpc:QC_OUT_2:1</r:URN>
   <r:TypeOfObject>OutParameter</r:TypeOfObject>
  </r:SourceParameterReference>
  <r:TargetParameterReference isReference="true" isExternal="false" lateBound="false">
   <r:URN>urn:ddi:us.mpc:GI_Age:1</r:URN>
   <r:TypeOfObject>InParameter</r:TypeOfObject>
  </r:TargetParameterReference>
 </r:Binding>
